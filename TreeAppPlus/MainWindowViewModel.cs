﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;

namespace TreeAppPlus
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string _xmlLinkString;
        public string XmlLinkString
        {
            get { return _xmlLinkString; }
            set { _xmlLinkString = value; OnPropertyChanged("XmlLinkString"); }
        }
        private string _filename;
        public string Filename
        {
            get { return _filename; }
            set { _filename = value; OnPropertyChanged("Filename"); }
        }
       
        public ICommand DownloadCommand { get; }
        public ICommand OpenCommand { get; }
        public MainWindowViewModel(TreeView xmlTreeView)
        {
            DownloadCommand = new RelayCommand(command => XmlDownloadFile(xmlTreeView));
            OpenCommand = new RelayCommand(command => XmlOpenFile(xmlTreeView));
        }
        public void XmlDownloadFile(TreeView xmlTreeView)
        {
            if (XmlLinkString != null && XmlLinkString.Split('.').Last() == "xml")
            {
                Filename = XmlLinkString.Split('/').Last();
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.FileName = Filename;
                bool? result = saveFile.ShowDialog();
                if (result == true)
                {
                    using (WebClient client = new WebClient())
                    {
                        byte[] fileData = client.DownloadData(XmlLinkString);
                        using (FileStream file = File.Create(saveFile.FileName))
                        {
                            file.Write(fileData, 0, fileData.Length);
                            file.Close();
                        }
                    }
                }
                xmlTreeView.Items.Clear();
                BuildTree(xmlTreeView, XDocument.Load(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), Filename)));
            }
            else
            {
                MessageBox.Show("Вставьте ссылку на xml файл");
                return;
            }
           
        }
        public void XmlOpenFile(TreeView xmlTreeView)
        {
            xmlTreeView.Items.Clear();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            bool? result = openFileDialog.ShowDialog();
            if (result == true)
            {
                string filePath = openFileDialog.FileName;
                string fileName = filePath.Split('\\').Last();
                using (FileStream fs = File.OpenRead(fileName))
                {
                    BuildTree(xmlTreeView, XDocument.Load(filePath));
                }
            }
        }
        private void BuildTree(TreeView treeView, XDocument doc)
        {
            TreeViewItem treeItem = new TreeViewItem
            {
                Header = doc.Root.Name.LocalName,
                IsExpanded = true
            };
            treeView.Items.Add(treeItem);
            BuildItems(treeItem, doc.Root);
        }
        private void BuildItems(TreeViewItem treeItem, XElement element)
        {
            foreach (XNode node in element.Nodes())
            {
                switch (node.NodeType)
                {
                    case XmlNodeType.Element:
                        XElement nodeElement = node as XElement;
                        string attributes = null;
                        if(nodeElement.HasAttributes)
                        {
                            attributes = null;
                            foreach(var n in nodeElement.Attributes())
                            {
                                attributes += n.Name + "='" + n.Value + "' ";
                            }
                        }
                        TreeViewItem viewItem = new TreeViewItem
                        {
                            Header = nodeElement.Name.LocalName + " " + attributes,
                            IsExpanded = true
                        };
                        treeItem.Items.Add(viewItem);
                        BuildItems(viewItem, nodeElement);
                        break;
                    case XmlNodeType.Text:
                        XText xText = node as XText;
                        treeItem.Items.Add(new TreeViewItem { Header = xText.Value, });
                        break;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
